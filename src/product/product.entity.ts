import { randomInt } from "crypto";
import { Factory } from "nestjs-seeder";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('Product')
export class product{
    @PrimaryGeneratedColumn()
    id: string;
    @Factory(faker => faker.lorem.words(2))
    @Column()
    title: string; 
    @Factory(faker => faker.lorem.words(2))
    @Column()
    description: string;
    @Factory(faker => faker.image.imageUrl())
    @Column()
    image: string;
    @Factory(faker => randomInt(20,100))
    @Column()
    price: string;
    @Factory(faker => faker.lorem.words(20))
    @Column()
    tensanpham: string;

}