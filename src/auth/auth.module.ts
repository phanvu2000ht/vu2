import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ProductService } from 'src/product/product.service';
import { UserModule } from 'src/user/user.module';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.statregy';

@Module({
  imports: [UserModule, PassportModule],
  providers: [AuthService, LocalStrategy],
  
})
export class AuthModule {}
