import { Controller, Request, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private userservice : UserService ){}
    @UseGuards(AuthGuard('local'))
    @Post('auth/login')
    async login(@Request() req): Promise<any> {
    return req.user;
  }
}
