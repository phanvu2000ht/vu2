import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { product } from './product.entity';

@Injectable()
export class ProductService {
constructor(
    @InjectRepository(product) private readonly productResponsitory: Repository<product> 
){}

async all(): Promise<product[]>{
    return this.productResponsitory.find()
}
async queryBuilder(alias: string){ 
    return this.productResponsitory.createQueryBuilder(alias);
}

}


