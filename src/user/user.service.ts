import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
export type User = {
    id : Number;
    name: string;
    username: string;
    password: string;
};
@Injectable()
export class UserService {
    private readonly users: User[] = [
        {
            id: 1,
            name: "phanvu",
            username: "vupro",
            password: "socute"
        },
        {
            id: 2,
            name: "phanvu2",
            username: "vupro2",
            password: "socute"
        },
        {
            id: 3,
            name: "phanvu3",
            username: "vupro3",
            password: "socute"
        },
        {
            id: 4,
            name: "phanvu4",
            username: "vupro4",
            password: "socute"
        },
    ];
    async findOne(username: string): Promise<User | undefined>{
        return this.users.find(user => user.username == username );
    }
}
