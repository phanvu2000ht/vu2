import { InjectRepository } from "@nestjs/typeorm";
import { DataFactory, Seeder} from "nestjs-seeder";
import { Repository } from "typeorm";
import { product } from "./product.entity";

export class ProductSeeder implements Seeder {
    constructor(
        @InjectRepository(product) private readonly productResponsitory: Repository<product> 
    ){}
    seed(): Promise<any> {
        const Product = DataFactory.createForClass(product).generate(50);
        return this.productResponsitory.insert(Product);
    }
    drop(): Promise<any> {
        return this.productResponsitory.delete([]);
    }


}