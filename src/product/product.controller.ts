import { Controller, Get, Req } from '@nestjs/common';
import { get } from 'http';
import { pathToFileURL } from 'url';
import { ProductService } from './product.service';
import { Request } from 'express';

@Controller('api/product')
export class ProductController {
    constructor(private ProductService: ProductService) {
        
    }
    @Get('frontend')
    async frontend(){
        return this.ProductService.all()
    }
    @Get("backend")
    async backend(@Req() req: Request){
        const build = await this.ProductService.queryBuilder('product');
        if(req.query.s){
            build.where("product.id LIKE :s", {s: '%{req.query.s}%'})
            }
        const sort: any = req.query.sort
        if(sort){
            build.orderBy("product.price", sort.toUpperCase())
        }
        
        return await build.getMany();
    }

}
