import { TypeOrmModule } from "@nestjs/typeorm";
import { seeder } from "nestjs-seeder";
import { ProductSeeder } from "./producr.seeder";
import { product } from "./product.entity";


seeder( {
    imports: [
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'vu2',
          password: 'vu2',
          database: 'test',
          entities: [product],
          synchronize: false,
          migrationsRun: false,
        }),
        TypeOrmModule.forFeature([product])
      ],
}).run([ProductSeeder])
