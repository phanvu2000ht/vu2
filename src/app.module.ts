import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { product } from './product/product.entity';
import { ProductModule } from './product/product.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'vu2',
      password: 'vu2',
      database: 'test',
      entities: [product],
      synchronize: false,
      migrations: [
        'db/migrations/*.js'
      ],
      cli:{
        migrationsDir: 'migrations'
      }
    }),
    ProductModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
